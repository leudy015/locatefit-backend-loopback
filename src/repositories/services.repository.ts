import {DefaultCrudRepository} from '@loopback/repository';
import {Services, ServicesRelations} from '../models';
import {LocatefitdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ServicesRepository extends DefaultCrudRepository<
  Services,
  typeof Services.prototype.id,
  ServicesRelations
> {
  constructor(
    @inject('datasources.locatefitdb') dataSource: LocatefitdbDataSource,
  ) {
    super(Services, dataSource);
  }
}
