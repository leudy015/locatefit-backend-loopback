import {DefaultCrudRepository} from '@loopback/repository';
import {Register, RegisterRelations} from '../models';
import {LocatefitdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RegisterRepository extends DefaultCrudRepository<
  Register,
  typeof Register.prototype.profesion,
  RegisterRelations
> {
  constructor(
    @inject('datasources.locatefitdb') dataSource: LocatefitdbDataSource,
  ) {
    super(Register, dataSource);
  }
}
