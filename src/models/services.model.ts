import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class Services extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'number',
    required: true,
    default: 0,
  })
  precio: number;

  @property({
    type: 'string',
  })
  corrency?: string;

  @property({
    type: 'date',
    required: true,
  })
  duracion: string;

  @property({
    type: 'string',
    required: true,
  })
  categoria: string;

  @property({
    type: 'string',
    required: true,
  })
  ciudad: string;

  @property({
    type: 'string',
    required: true,
  })
  descripcion: string;

  @property({
    type: 'string',
  })
  lunes?: string;

  @property({
    type: 'date',
  })
  lunes_a?: string;

  @property({
    type: 'date',
  })
  lunes_to?: string;

  @property({
    type: 'string',
  })
  martes?: string;

  @property({
    type: 'date',
  })
  martes_a?: string;

  @property({
    type: 'date',
  })
  martes_to?: string;

  @property({
    type: 'string',
  })
  miercoles?: string;

  @property({
    type: 'date',
  })
  miercoles_a?: string;

  @property({
    type: 'date',
  })
  miercoles_to?: string;

  @property({
    type: 'string',
  })
  jueves?: string;

  @property({
    type: 'date',
  })
  jueves_a?: string;

  @property({
    type: 'date',
  })
  jueves_to?: string;

  @property({
    type: 'string',
  })
  viernes?: string;

  @property({
    type: 'date',
  })
  viernes_a?: string;

  @property({
    type: 'date',
  })
  viernes_to?: string;

  @property({
    type: 'string',
  })
  sabado?: string;

  @property({
    type: 'date',
  })
  sabado_a?: string;

  @property({
    type: 'date',
  })
  sabado_to?: string;

  @property({
    type: 'string',
  })
  domingo?: string;

  @property({
    type: 'date',
  })
  domingo_a?: string;

  @property({
    type: 'date',
  })
  domingo_to?: string;


  constructor(data?: Partial<Services>) {
    super(data);
  }
}

export interface ServicesRelations {
  // describe navigational properties here
}

export type ServicesWithRelations = Services & ServicesRelations;
