import { Entity, model, property } from '@loopback/repository';

@model({ settings: { strict: false } })
export class Register extends Entity {
  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  lastname?: string;

  @property({
    type: 'string',
  })
  profesion?: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
  })
  telefono?: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  city?: string;

  @property({
    type: 'date',
  })
  datebrithday?: string;

  @property({
    type: 'number',
    default: 0,
  })
  experiencie?: number;

  @property({
    type: 'string',
  })
  stude?: string;

  @property({
    type: 'string',
  })
  facebook?: string;

  @property({
    type: 'string',
  })
  twitter?: string;

  @property({
    type: 'string',
  })
  instagram?: string;

  @property({
    type: 'string',
  })
  youtube?: string;

  @property({
    type: 'string',
  })
  web?: string;

  @property({
    type: 'string',
  })
  country?: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Register>) {
    super(data);
  }
}

export interface RegisterRelations {
  // describe navigational properties here
}

export type RegisterWithRelations = Register & RegisterRelations;
