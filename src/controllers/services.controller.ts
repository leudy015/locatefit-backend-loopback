import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Services} from '../models';
import {ServicesRepository} from '../repositories';

export class ServicesController {
  constructor(
    @repository(ServicesRepository)
    public servicesRepository : ServicesRepository,
  ) {}

  @post('/services', {
    responses: {
      '200': {
        description: 'Services model instance',
        content: {'application/json': {schema: getModelSchemaRef(Services)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Services, {
            title: 'NewServices',
            exclude: ['id'],
          }),
        },
      },
    })
    services: Omit<Services, 'id'>,
  ): Promise<Services> {
    return this.servicesRepository.create(services);
  }

  @get('/services/count', {
    responses: {
      '200': {
        description: 'Services model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Services)) where?: Where<Services>,
  ): Promise<Count> {
    return this.servicesRepository.count(where);
  }

  @get('/services', {
    responses: {
      '200': {
        description: 'Array of Services model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Services)},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Services)) filter?: Filter<Services>,
  ): Promise<Services[]> {
    return this.servicesRepository.find(filter);
  }

  @patch('/services', {
    responses: {
      '200': {
        description: 'Services PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Services, {partial: true}),
        },
      },
    })
    services: Services,
    @param.query.object('where', getWhereSchemaFor(Services)) where?: Where<Services>,
  ): Promise<Count> {
    return this.servicesRepository.updateAll(services, where);
  }

  @get('/services/{id}', {
    responses: {
      '200': {
        description: 'Services model instance',
        content: {'application/json': {schema: getModelSchemaRef(Services)}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Services> {
    return this.servicesRepository.findById(id);
  }

  @patch('/services/{id}', {
    responses: {
      '204': {
        description: 'Services PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Services, {partial: true}),
        },
      },
    })
    services: Services,
  ): Promise<void> {
    await this.servicesRepository.updateById(id, services);
  }

  @put('/services/{id}', {
    responses: {
      '204': {
        description: 'Services PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() services: Services,
  ): Promise<void> {
    await this.servicesRepository.replaceById(id, services);
  }

  @del('/services/{id}', {
    responses: {
      '204': {
        description: 'Services DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.servicesRepository.deleteById(id);
  }
}
