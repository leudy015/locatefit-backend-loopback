import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Register} from '../models';
import {RegisterRepository} from '../repositories';

export class RegisterController {
  constructor(
    @repository(RegisterRepository)
    public registerRepository : RegisterRepository,
  ) {}

  @post('/registers', {
    responses: {
      '200': {
        description: 'Register model instance',
        content: {'application/json': {schema: getModelSchemaRef(Register)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Register, {
            title: 'NewRegister',
            exclude: ['id'],
          }),
        },
      },
    })
    register: Omit<Register, 'id'>,
  ): Promise<Register> {
    return this.registerRepository.create(register);
  }

  @get('/registers/count', {
    responses: {
      '200': {
        description: 'Register model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Register)) where?: Where<Register>,
  ): Promise<Count> {
    return this.registerRepository.count(where);
  }

  @get('/registers', {
    responses: {
      '200': {
        description: 'Array of Register model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Register)},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Register)) filter?: Filter<Register>,
  ): Promise<Register[]> {
    return this.registerRepository.find(filter);
  }

  @patch('/registers', {
    responses: {
      '200': {
        description: 'Register PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Register, {partial: true}),
        },
      },
    })
    register: Register,
    @param.query.object('where', getWhereSchemaFor(Register)) where?: Where<Register>,
  ): Promise<Count> {
    return this.registerRepository.updateAll(register, where);
  }

  @get('/registers/{id}', {
    responses: {
      '200': {
        description: 'Register model instance',
        content: {'application/json': {schema: getModelSchemaRef(Register)}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Register> {
    return this.registerRepository.findById(id);
  }

  @patch('/registers/{id}', {
    responses: {
      '204': {
        description: 'Register PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Register, {partial: true}),
        },
      },
    })
    register: Register,
  ): Promise<void> {
    await this.registerRepository.updateById(id, register);
  }

  @put('/registers/{id}', {
    responses: {
      '204': {
        description: 'Register PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() register: Register,
  ): Promise<void> {
    await this.registerRepository.replaceById(id, register);
  }

  @del('/registers/{id}', {
    responses: {
      '204': {
        description: 'Register DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.registerRepository.deleteById(id);
  }
}
